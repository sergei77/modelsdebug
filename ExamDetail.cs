﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CsJobHunter.Core.Models
{
    public class ExamDetail
    {
        public int Id { get; set; }

        [Required]
        public int ExamId { get; set; }
        [Required]
        public Exam Exam { get; set; }

        public int? QuestionId { get; set; }

        [ForeignKey("QuestionId")]
        public Question Question { get; set; }

        
        public int? AnswerId { get; set; }
        [ForeignKey("AnswerId")]
        public Answer Answer { get; set; }

        public DateTime DateTimeBegin { get; set; }
        public DateTime DateTimeEnd { get; set; } // TO DO проверка, что DateTimeEnd > DateTimeBegin.
    }
}

