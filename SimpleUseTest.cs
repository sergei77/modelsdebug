﻿using System;
using System.Linq;

namespace CsJobHunter.Core.Models
{
    static class SimpleUseTest
    {
        // При изменении БД требует миграции. Пока даже без предупреждений и переспрашивания.
        public static void DeleteDB()
        {
            using CSJobHunterContext db = new CSJobHunterContext();
            db.Database.Delete();
            Console.WriteLine("DataBase deleted");
        }

        public static void ClearDBTables()
        {
            using CSJobHunterContext db = new CSJobHunterContext();
            db.Students.RemoveRange(db.Students);
            db.Exams.RemoveRange(db.Exams);
            db.ExamDetails.RemoveRange(db.ExamDetails);
            db.Questions.RemoveRange(db.Questions);
            db.Answers.RemoveRange(db.Answers);
            db.SaveChanges();
        }

        public static void ShowQuestionsAndAnswersToThem(CSJobHunterContext db)
        {
            var questions = db.Questions;
            Console.WriteLine("\nОбщий список вопросов и ответов:");
            foreach (Question qstn in questions)
            {
                Console.WriteLine($"  Id:{qstn.Id}   {qstn.Content}");
                if (!String.IsNullOrEmpty(qstn.CodeSimple))
                    Console.WriteLine(qstn.CodeSimple);

                db.Entry(qstn).Collection("Answers").Load();
                foreach (var ans in qstn.Answers)
                    Console.WriteLine($"  Id:{ans.Id}     {ans}");

                Console.WriteLine("-----------------------");
            }
        }

        public static void ShowStudentsAndTheirExams(CSJobHunterContext db)
        {
            var students = db.Students;
            Console.WriteLine("Список студентов в таблице Students:");
            foreach (Student st in students)
            {
                Console.WriteLine("{0}.  {1} - Login: {2}  e-mail: {3}", st.Id, st.FirstName, st.Login, st.Email);
                db.Entry(st).Collection("Exams").Load();
                foreach (var exam in st.Exams)
                {
                    Console.WriteLine($"      {exam.Id}: Exam result = {exam.Mark}");
                    db.Entry(exam).Collection("ExamDetails").Load();
                    foreach (var examDet in exam.ExamDetails)
                    {
                        var question = db.Questions.Find(examDet.QuestionId);
                        var answer = db.Answers.Find(examDet.AnswerId);
                        Console.WriteLine($"            QestionId: {examDet.QuestionId}  {question.Content}");
                        Console.WriteLine($"            AnswerId:  {examDet.AnswerId}    {answer.Content}");
                    }

                }
            }
        }

        public static void AddStudentsToDB()
        {
            // Две строки в таблицу Students
            using CSJobHunterContext db = new CSJobHunterContext();
            db.Students.RemoveRange(db.Students);

            Student student1 = new Student() { FirstName = "Первый", LastName = "Василий", Email = "firstStudentTestEmail@gmail.com", Login = "1", Password = "1" };
            Student student2 = new Student() { FirstName = "Второй", LastName = "Пётр", Email = "secondStudentTestEmail@yandex.ru", Login = "2", Password = "2" };

            db.Students.Add(student1);
            db.Students.Add(student2);

            db.SaveChanges();
        }

        public static void AddQuestionsAndTheirsAnswersToDB()
        {
            // TO DO транзакции, чтобы вопрос добавлялся вместе с ответами на него

            // Четыре вопроса и ответы на него в таблицы Question и Answers            
            using CSJobHunterContext db = new CSJobHunterContext();
            Question question = new Question() { Content = "Что такое int?" };
            db.Questions.Add(question);
            db.Answers.Add(new Answer() { Content = "Тип данных", IsRight = true, Question = question });
            db.Answers.Add(new Answer() { Content = "Класс", IsRight = false, Question = question });
            db.Answers.Add(new Answer() { Content = "Интерфейс", IsRight = false, Question = question });

            question = new Question() { Content = "Что будет содержать строка name после выполнения данного кода?", CodeSimple = "string name = \"Hello\";\nname[1] = '.';" };
            db.Questions.Add(question);
            db.Answers.Add(new Answer() { Content = "Hello", IsRight = false, Question = question });
            db.Answers.Add(new Answer() { Content = "H.llo", IsRight = false, Question = question });
            db.Answers.Add(new Answer() { Content = ".ello", IsRight = false, Question = question });
            db.Answers.Add(new Answer() { Content = "Код не скомпилируется", IsRight = true, Question = question });

            question = new Question() { Content = "Может ли класс реализовывать несколько интерфейсов?" };
            db.Questions.Add(question);
            db.Answers.Add(new Answer() { Content = "Нет, в C# не поддерживается множественное наследование", IsRight = false, Question = question });
            db.Answers.Add(new Answer() { Content = "Да может", IsRight = true, Question = question });


            question = new Question() { Content = "Отметьте правильные ответы.\nИнтерфейс может содержать:" };
            db.Questions.Add(question);
            db.Answers.Add(new Answer() { Content = "Абстрактные методы", IsRight = true, Question = question });
            db.Answers.Add(new Answer() { Content = "Поля", IsRight = false, Question = question });
            db.Answers.Add(new Answer() { Content = "Реализации объявленных методов", IsRight = false, Question = question });
            db.Answers.Add(new Answer() { Content = "Индексаторы", IsRight = false, Question = question });

            db.SaveChanges();
        }

        public static void Student1ToPassExam()
        {
            using CSJobHunterContext db = new CSJobHunterContext();
            //var student1 = db.Students.Where(st => st.Id == 1);
            var student1 = db.Students.FirstOrDefault();
            if (student1 != null)
            {
                DateTime examBegin = new DateTime(2021, 4, 3, 14, 04, 25); // год - месяц - день - час - минута - секунда.
                Exam student1exam1 = new Exam() { DateTimeBegin = examBegin, DateTimeEnd = examBegin, Student = student1 };

                Question question1 = db.Questions.First();  // без проверки.

                db.Entry(question1).Collection("Answers").Load();
                Answer[] answers = question1.Answers.ToArray();

                // Пусть был выбран ответ №2                
                db.ExamDetails.Add(new ExamDetail() { Exam = student1exam1, QuestionId = question1.Id, AnswerId = answers[1].Id, DateTimeBegin = examBegin.AddMinutes(1), DateTimeEnd = examBegin.AddMinutes(1) });

                student1exam1.DateTimeEnd = examBegin.AddMinutes(2);
                student1exam1.Mark = TMark.Fail;
                db.Exams.Add(student1exam1);
                db.SaveChanges();
            }
        }


        public static void Student2ToPassExam()
        {
            using CSJobHunterContext db = new CSJobHunterContext();
            var students = db.Students.ToList();
            if (students.Count >= 2)
            {
                Student student2 = students[1];
                DateTime examBegin = new DateTime(2021, 4, 3, 15, 26, 25); // год - месяц - день - час - минута - секунда.
                Exam student2exam = new Exam() { DateTimeBegin = examBegin, DateTimeEnd = examBegin, Student = student2 };

                Question question1 = db.Questions.First();  // без проверки.

                db.Entry(question1).Collection("Answers").Load();
                Answer[] answers = question1.Answers.ToArray();

                // Пусть был выбран ответ №1                
                db.ExamDetails.Add(new ExamDetail() { Exam = student2exam, QuestionId = question1.Id, AnswerId = answers[0].Id, DateTimeBegin = examBegin.AddMinutes(1), DateTimeEnd = examBegin.AddMinutes(1) });

                Question question2 = db.Questions.ToArray()[1];  // без проверки.

                db.Entry(question2).Collection("Answers").Load();
                answers = question2.Answers.ToArray();

                // Пусть был выбран ответ №4                
                db.ExamDetails.Add(new ExamDetail() { Exam = student2exam, QuestionId = question2.Id, AnswerId = answers[3].Id, DateTimeBegin = examBegin.AddMinutes(3), DateTimeEnd = examBegin.AddMinutes(3) });

                student2exam.DateTimeEnd = examBegin.AddMinutes(2);
                student2exam.Mark = TMark.Excellent;
                db.Exams.Add(student2exam);
                db.SaveChanges();
            }
        }

        public static void Student1ToPassAnotherExam()
        {
            using CSJobHunterContext db = new CSJobHunterContext();
            var student1 = db.Students.FirstOrDefault();
            if (student1 != null)
            {
                DateTime examBegin = new DateTime(2021, 4, 3, 15, 46, 25); // год - месяц - день - час - минута - секунда.
                Exam student1exam2 = new Exam() { DateTimeBegin = examBegin, DateTimeEnd = examBegin, Student = student1 };

                Question[] questions = db.Questions.ToArray();

                if (questions.Length >= 4)
                {
                    Question question1 = questions[2];

                    db.Entry(question1).Collection("Answers").Load();
                    Answer[] answers = question1.Answers.ToArray();

                    // Пусть был выбран ответ №2                
                    db.ExamDetails.Add(new ExamDetail() { Exam = student1exam2, QuestionId = question1.Id, AnswerId = answers[1].Id, DateTimeBegin = examBegin.AddMinutes(1), DateTimeEnd = examBegin.AddMinutes(1) });

                    Question question2 = questions[3];

                    db.Entry(question2).Collection("Answers").Load();
                    answers = question2.Answers.ToArray();

                    // Пусть был выбран ответ №2                
                    db.ExamDetails.Add(new ExamDetail() { Exam = student1exam2, QuestionId = question2.Id, AnswerId = answers[1].Id, DateTimeBegin = examBegin.AddMinutes(3), DateTimeEnd = examBegin.AddMinutes(3) });

                    student1exam2.DateTimeEnd = examBegin.AddMinutes(2);
                    student1exam2.Mark = TMark.Satisfactory;
                    db.Exams.Add(student1exam2);
                    db.SaveChanges();
                }
            }
        }

        public static void SetAndShowTestDataToTables()
        {
            ClearDBTables();
            AddStudentsToDB();
            AddQuestionsAndTheirsAnswersToDB();

            Student1ToPassExam();
            Student2ToPassExam();
            Student1ToPassAnotherExam();

            using CSJobHunterContext db = new CSJobHunterContext();
            ShowStudentsAndTheirExams(db);
            ShowQuestionsAndAnswersToThem(db);
        }
    }
}
