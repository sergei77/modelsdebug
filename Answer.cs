﻿using System.ComponentModel.DataAnnotations;

namespace CsJobHunter.Core.Models
{
    public class Answer
    {
        public int Id { get; set; }

        [MaxLength(256), Required(ErrorMessage = "Answer must have filled Content.")]        
        public string Content { get; set; }

        [Required]
        public bool IsRight { get; set; }

        [Required]
        public int QuestionId { get; set; }

        [Required]
        public Question Question { get; set; }

        public int? ExamDetailId { get; set; }

        public override string ToString()
        {
            if (IsRight)
                return "+ " + Content;
            else
                return "  " + Content;
        }
    }
}

