﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CsJobHunter.Core.Models
{
    public class Exam
    {
        public int Id { get; set; }
        public DateTime DateTimeBegin { get; set; }
        public DateTime DateTimeEnd { get; set; } // TO DO проверка, что DateTimeEnd > DateTimeBegin.

        [Range(1, 5, ErrorMessage = "Result must be between 1 and 5.")]
        public TMark? Mark { get; set; }
        public bool? IsPassed { get; set; }

        [Required]
        public int StudentId { get; set; }
        [Required]
        public Student Student { get; set; }

        public ICollection<ExamDetail> ExamDetails { get; set; }
    }
}