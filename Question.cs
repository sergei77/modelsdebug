﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CsJobHunter.Core.Models
{
    public class Question
    {
        public int Id { get; set; }

        [MaxLength(256), Required(ErrorMessage = "Question must have filled Content.")]
        public string Content { get; set; }

        [MaxLength(256)]
        public string CodeSimple { get; set; }
        public ICollection<Answer> Answers { get; set; }
        
        public int ExamDetailId { get; set; }
    }
}

