﻿using System;
using System.Data.Entity;

namespace CsJobHunter.Core.Models
{
    // https://docs.microsoft.com/ru-ru/ef/core/modeling/relationships?tabs=fluent-api%2Cfluent-api-simple-key%2Csimple-key
/*
    В Answer
    public override string ToString() оставил
    без инициализации DateTimeEnd в объектах вылетало, присваивал те же значения, что и для DateTimeBegin
*/

    public class CSJobHunterContext : DbContext
    {
        public CSJobHunterContext() : base("CSTestContext")
        { }

        public DbSet<Student> Students { get; set; }
        public DbSet<Exam> Exams { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<ExamDetail> ExamDetails { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);
        //    modelBuilder.Entity<ExamDetail>()
        //        .HasRequired(p => p.Question)
        //        .WithOptional(c => c.ExamDetail);
        //}
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Запустился...");
            SimpleUseTest.DeleteDB();

            SimpleUseTest.SetAndShowTestDataToTables();

            Console.WriteLine("Нажмите Enter для завершения.");
            Console.ReadLine();
        }
    }
}

