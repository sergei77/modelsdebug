﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CsJobHunter.Core.Models
{
    public class Student
    {
        public int Id { get; set; }

        [MaxLength(50), Required(ErrorMessage = "FirstName is required.")]
        public string FirstName { get; set; }

        [MaxLength(100)]
        public string LastName { get; set; }

        [MaxLength(100)]
        public string Email { get; set; }

        [MaxLength(50), Required(ErrorMessage = "Login is required.")]
        public string Login { get; set; }

        [MaxLength(50)]
        public string Password { get; set; }

        public ICollection<Exam> Exams { get; set; }
    }
}

